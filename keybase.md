### Keybase proof

I hereby claim:

  * I am zemanel on github.
  * I am zemanel (https://keybase.io/zemanel) on keybase.
  * I have a public key ASCV1nDX6cVt3TS8-VfKIrByKac4Hjo9hR9p_6YeV_ielwo

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "012095d670d7e9c56ddd34bcf957ca22b07229a7381e3a3d851f69ffa61e57f89e970a",
      "host": "keybase.io",
      "kid": "012095d670d7e9c56ddd34bcf957ca22b07229a7381e3a3d851f69ffa61e57f89e970a",
      "uid": "04521efd0ec99265b71d70fd8afae119",
      "username": "zemanel"
    },
    "merkle_root": {
      "ctime": 1509548940,
      "hash": "c5e4217e76a0835d597100614046499a3ae46e6e152b54083ca9daf9ba819f66a716ed455ce33025f572d23a23cada7abcd96a8841d188eae431900eca04a6a4",
      "hash_meta": "ad32ec5d6ce55dedc4eedaf4e4fa2c55b073b506271001adea976ec2f9d59c60",
      "seqno": 1655183
    },
    "service": {
      "name": "github",
      "username": "zemanel"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "client": {
    "name": "keybase.io go client",
    "version": "1.0.34"
  },
  "ctime": 1509548965,
  "expire_in": 504576000,
  "prev": "9f11b2cc5116e85f2a47bf44862166ff3f5aace7a9bed3f34ba28f58183a00e7",
  "seqno": 21,
  "tag": "signature"
}
```

with the key [ASCV1nDX6cVt3TS8-VfKIrByKac4Hjo9hR9p_6YeV_ielwo](https://keybase.io/zemanel), yielding the signature:

```
hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgldZw1+nFbd00vPlXyiKwcimnOB46PYUfaf+mHlf4npcKp3BheWxvYWTFAzt7ImJvZHkiOnsia2V5Ijp7ImVsZGVzdF9raWQiOiIwMTIwOTVkNjcwZDdlOWM1NmRkZDM0YmNmOTU3Y2EyMmIwNzIyOWE3MzgxZTNhM2Q4NTFmNjlmZmE2MWU1N2Y4OWU5NzBhIiwiaG9zdCI6ImtleWJhc2UuaW8iLCJraWQiOiIwMTIwOTVkNjcwZDdlOWM1NmRkZDM0YmNmOTU3Y2EyMmIwNzIyOWE3MzgxZTNhM2Q4NTFmNjlmZmE2MWU1N2Y4OWU5NzBhIiwidWlkIjoiMDQ1MjFlZmQwZWM5OTI2NWI3MWQ3MGZkOGFmYWUxMTkiLCJ1c2VybmFtZSI6InplbWFuZWwifSwibWVya2xlX3Jvb3QiOnsiY3RpbWUiOjE1MDk1NDg5NDAsImhhc2giOiJjNWU0MjE3ZTc2YTA4MzVkNTk3MTAwNjE0MDQ2NDk5YTNhZTQ2ZTZlMTUyYjU0MDgzY2E5ZGFmOWJhODE5ZjY2YTcxNmVkNDU1Y2UzMzAyNWY1NzJkMjNhMjNjYWRhN2FiY2Q5NmE4ODQxZDE4OGVhZTQzMTkwMGVjYTA0YTZhNCIsImhhc2hfbWV0YSI6ImFkMzJlYzVkNmNlNTVkZWRjNGVlZGFmNGU0ZmEyYzU1YjA3M2I1MDYyNzEwMDFhZGVhOTc2ZWMyZjlkNTljNjAiLCJzZXFubyI6MTY1NTE4M30sInNlcnZpY2UiOnsibmFtZSI6ImdpdGh1YiIsInVzZXJuYW1lIjoiemVtYW5lbCJ9LCJ0eXBlIjoid2ViX3NlcnZpY2VfYmluZGluZyIsInZlcnNpb24iOjF9LCJjbGllbnQiOnsibmFtZSI6ImtleWJhc2UuaW8gZ28gY2xpZW50IiwidmVyc2lvbiI6IjEuMC4zNCJ9LCJjdGltZSI6MTUwOTU0ODk2NSwiZXhwaXJlX2luIjo1MDQ1NzYwMDAsInByZXYiOiI5ZjExYjJjYzUxMTZlODVmMmE0N2JmNDQ4NjIxNjZmZjNmNWFhY2U3YTliZWQzZjM0YmEyOGY1ODE4M2EwMGU3Iiwic2Vxbm8iOjIxLCJ0YWciOiJzaWduYXR1cmUifaNzaWfEQP5gbzTaGTZWLNarRwebVfXZNxAW4QYDJYD6t3t8In3rEpXk+uWin6fK9HwdkRtuy1tabAIA2C9CXIiNFGsWlQKoc2lnX3R5cGUgpGhhc2iCpHR5cGUIpXZhbHVlxCCtHNDyvrQtcXpajW9fKKw1K86RdlwA7/wGMQtj9uIbZ6N0YWfNAgKndmVyc2lvbgE=

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/zemanel

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id zemanel
```